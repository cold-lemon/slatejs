"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
exports.__esModule = true;
var react_1 = require("react");
var is_hotkey_1 = require("is-hotkey");
var image_extensions_1 = require("image-extensions");
var slate_react_1 = require("slate-react");
var slate_1 = require("slate");
var slate_history_1 = require("slate-history");
var components_1 = require("./components");
var is_url_1 = require("is-url");
var emotion_1 = require("emotion");
var antd_1 = require("antd");
require("./index.css");
var HOTKEYS = {
    'mod+b': 'bold',
    'mod+i': 'italic',
    'mod+u': 'underline',
    'mod+`': 'code'
};
var LIST_TYPES = ['numbered-list', 'bulleted-list'];
var RichText = function () {
    var _a = react_1.useState(initialValue), value = _a[0], setValue = _a[1];
    var renderElement = react_1.useCallback(function (props) { return react_1["default"].createElement(Element, __assign({}, props)); }, []);
    var renderLeaf = react_1.useCallback(function (props) { return react_1["default"].createElement(Leaf, __assign({}, props)); }, []);
    var editor = react_1.useMemo(function () { return withEmbeds(withImages(slate_history_1.withHistory(slate_react_1.withReact(slate_1.createEditor())))); }, []);
    return (react_1["default"].createElement("div", { className: "rich-text " },
        react_1["default"].createElement(slate_react_1.Slate, { classNmae: "slate", editor: editor, value: value, onChange: function (value) {
                console.log(value);
                setValue(value);
            } },
            react_1["default"].createElement("div", { className: "toolbar" },
                react_1["default"].createElement(components_1.Toolbar, { className: "tool-bar" },
                    react_1["default"].createElement(MarkButton, { format: "bold", icon: "format_bold" }),
                    react_1["default"].createElement(MarkButton, { format: "italic", icon: "format_italic" }),
                    react_1["default"].createElement(MarkButton, { format: "underline", icon: "format_underlined" }),
                    react_1["default"].createElement(MarkButton, { format: "code", icon: "code" }),
                    react_1["default"].createElement(BlockButton, { format: "heading-one", icon: "looks_one" }),
                    react_1["default"].createElement(BlockButton, { format: "heading-two", icon: "looks_two" }),
                    react_1["default"].createElement(BlockButton, { format: "block-quote", icon: "format_quote" }),
                    react_1["default"].createElement(BlockButton, { format: "numbered-list", icon: "format_list_numbered" }),
                    react_1["default"].createElement(BlockButton, { format: "bulleted-list", icon: "format_list_bulleted" }),
                    react_1["default"].createElement(InsertImageButton, null),
                    react_1["default"].createElement(InsertVideoButton, null))),
            react_1["default"].createElement("div", { className: "editor-con" },
                react_1["default"].createElement(slate_react_1.Editable, { className: "editor", renderElement: renderElement, renderLeaf: renderLeaf, placeholder: "Enter some rich text\u2026", spellCheck: true, autoFocus: true, onKeyDown: function (event) {
                        for (var hotkey in HOTKEYS) {
                            if (is_hotkey_1["default"](hotkey, event)) {
                                event.preventDefault();
                                var mark = HOTKEYS[hotkey];
                                toggleMark(editor, mark);
                            }
                        }
                    } })))));
};
var withImages = function (editor) {
    var insertData = editor.insertData, isVoid = editor.isVoid;
    editor.isVoid = function (element) {
        return element.type === 'image' ? true : isVoid(element);
    };
    editor.insertData = function (data) {
        var text = data.getData('text/plain');
        var files = data.files;
        if (files && files.length > 0) {
            var _loop_1 = function (file) {
                var reader = new FileReader();
                var mime = file.type.split('/')[0];
                if (mime === 'image') {
                    reader.addEventListener('load', function () {
                        var url = reader.result;
                        insertImage(editor, url);
                    });
                    reader.readAsDataURL(file);
                }
            };
            for (var _i = 0, files_1 = files; _i < files_1.length; _i++) {
                var file = files_1[_i];
                _loop_1(file);
            }
        }
        else if (isImageUrl(text)) {
            insertImage(editor, text);
        }
        else {
            insertData(data);
        }
    };
    return editor;
};
var insertImage = function (editor, url) {
    var text = { text: '' };
    var image = { type: 'image', url: url, children: [text] };
    slate_1.Transforms.insertNodes(editor, image);
};
var ImageElement = function (_a) {
    var attributes = _a.attributes, children = _a.children, element = _a.element;
    console.log(element);
    var selected = slate_react_1.useSelected();
    var focused = slate_react_1.useFocused();
    return (react_1["default"].createElement("div", __assign({}, attributes),
        react_1["default"].createElement("div", { contentEditable: false },
            react_1["default"].createElement("img", { src: element.url, className: emotion_1.css(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n            display: block;\n            max-width: 100%;\n            max-height: 20em;\n            box-shadow: ", ";\n          "], ["\n            display: block;\n            max-width: 100%;\n            max-height: 20em;\n            box-shadow: ", ";\n          "])), selected && focused ? '0 0 0 3px #B4D5FF' : 'none') })),
        children));
};
var InsertImageButton = function () {
    var editor = slate_react_1.useEditor();
    var imgProps = {
        name: 'file',
        action: 'http://192.168.1.138:9991/api/uploadFile',
        accept: '.svg, .png, .bmp, .jpg, .jpeg, .gif, .tif, .tiff, .emf, .webp',
        showUploadList: false,
        onChange: function (info) {
            if (info.file.status === 'done') {
                if (info.file.response) {
                    console.log(info.file.response);
                    insertImage(editor, info.file.response.result);
                }
            }
        }
    };
    return (react_1["default"].createElement(antd_1.Upload, __assign({}, imgProps),
        react_1["default"].createElement(components_1.Button, { className: emotion_1.css(templateObject_2 || (templateObject_2 = __makeTemplateObject(["\n          z-index: 2;\n        "], ["\n          z-index: 2;\n        "]))), onMouseDown: function (event) {
                console.log(event);
                // insertImage(editor, url)
            } },
            react_1["default"].createElement(components_1.Icon, null, "image"))));
};
var isImageUrl = function (url) {
    if (!url)
        return false;
    if (!is_url_1["default"](url))
        return false;
    var ext = new URL(url).pathname.split('.').pop();
    return image_extensions_1["default"].includes(ext);
};
var InsertVideoButton = function () {
    var editor = slate_react_1.useEditor();
    var videoProps = {
        name: 'file',
        action: 'http://192.168.1.138:9991/api/uploadFile',
        accept: '.AVI, .mov, .rmvb, .rm, .FLV, .mp4, .3GP',
        showUploadList: false,
        headers: {
            authorization: 'authorization-text'
        },
        onChange: function (info) {
            // TODO上传文件后，获取地址，插入文档，JSON文档上传到服务器（LOCK）
            if (info.file.status === 'done') {
                if (info.file.response) {
                    var url = info.file.response.result;
                    var text = { text: '' };
                    var video = { type: 'video', url: url, children: [text] };
                    slate_1.Transforms.insertNodes(editor, video);
                }
            }
        }
    };
    return (react_1["default"].createElement(antd_1.Upload, __assign({}, videoProps),
        react_1["default"].createElement(components_1.Button, { onMouseDown: function (event) {
                // insertImage(editor, url)
            } },
            react_1["default"].createElement(components_1.Icon, null, "video_library"))));
};
var withEmbeds = function (editor) {
    var isVoid = editor.isVoid;
    editor.isVoid = function (element) {
        return element.type === 'video' ? true : isVoid(element);
    };
    return editor;
};
var VideoElement = function (_a) {
    var attributes = _a.attributes, children = _a.children, element = _a.element;
    var editor = slate_react_1.useEditor();
    var url = element.url;
    console.log(url);
    return (react_1["default"].createElement("div", __assign({}, attributes),
        react_1["default"].createElement("div", { contentEditable: false },
            react_1["default"].createElement("div", { style: {
                    // padding: '75% 0 0 0',
                    position: 'relative'
                } },
                react_1["default"].createElement("video", { preload: "true", "data-src": "https://lark-video.oss-cn-hangzhou.aliyuncs.com/outputs/prod/yuque/2020/1679004/mp4/1605579250604-b2e16107-3eef-4e19-93d2-7852206531a8.mp4?OSSAccessKeyId=LTAI4GGhPJmQ4HWCmhDAn4F5&Expires=1605586464&Signature=HxLDTS87TK8NSFGVod5Mu34XPGY%3D", 
                    // webkit-playsinline="webkit-playsinline"
                    // playsinline="true"
                    // class="lozad"
                    poster: url, controls: true, src: url, "data-loaded": "true", className: emotion_1.css(templateObject_3 || (templateObject_3 = __makeTemplateObject(["\n              width: 100%;\n              max-height: 450px;\n            "], ["\n              width: 100%;\n              max-height: 450px;\n            "]))) }))),
        children));
};
var UrlInput = function (_a) {
    var url = _a.url, onChange = _a.onChange;
    var _b = react_1["default"].useState(url), value = _b[0], setValue = _b[1];
    return (react_1["default"].createElement("input", { value: value, onClick: function (e) { return e.stopPropagation(); }, style: {
            marginTop: '5px',
            boxSizing: 'border-box'
        }, onChange: function (e) {
            var newUrl = e.target.value;
            setValue(newUrl);
            onChange(newUrl);
        } }));
};
var toggleBlock = function (editor, format) {
    var isActive = isBlockActive(editor, format);
    var isList = LIST_TYPES.includes(format);
    slate_1.Transforms.unwrapNodes(editor, {
        match: function (n) { return LIST_TYPES.includes(n.type); },
        split: true
    });
    slate_1.Transforms.setNodes(editor, {
        type: isActive ? 'paragraph' : isList ? 'list-item' : format
    });
    if (!isActive && isList) {
        var block = { type: format, children: [] };
        slate_1.Transforms.wrapNodes(editor, block);
    }
};
var toggleMark = function (editor, format) {
    var isActive = isMarkActive(editor, format);
    if (isActive) {
        slate_1.Editor.removeMark(editor, format);
    }
    else {
        slate_1.Editor.addMark(editor, format, true);
    }
};
var isBlockActive = function (editor, format) {
    var match = slate_1.Editor.nodes(editor, {
        match: function (n) { return n.type === format; }
    })[0];
    return !!match;
};
var isMarkActive = function (editor, format) {
    var marks = slate_1.Editor.marks(editor);
    return marks ? marks[format] === true : false;
};
var Element = function (props) {
    var attributes = props.attributes, children = props.children, element = props.element;
    switch (element.type) {
        case 'video':
            return react_1["default"].createElement(VideoElement, __assign({}, props));
        case 'image':
            return react_1["default"].createElement(ImageElement, __assign({}, props));
        case 'block-quote':
            return react_1["default"].createElement("blockquote", __assign({}, attributes), children);
        case 'bulleted-list':
            return react_1["default"].createElement("ul", __assign({}, attributes), children);
        case 'heading-one':
            return react_1["default"].createElement("h1", __assign({}, attributes), children);
        case 'heading-two':
            return react_1["default"].createElement("h2", __assign({}, attributes), children);
        case 'list-item':
            return react_1["default"].createElement("li", __assign({}, attributes), children);
        case 'numbered-list':
            return react_1["default"].createElement("ol", __assign({}, attributes), children);
        default:
            return react_1["default"].createElement("p", __assign({}, attributes), children);
    }
};
var Leaf = function (_a) {
    var attributes = _a.attributes, children = _a.children, leaf = _a.leaf;
    if (leaf.bold) {
        children = react_1["default"].createElement("strong", null, children);
    }
    if (leaf.code) {
        children = react_1["default"].createElement("code", null, children);
    }
    if (leaf.italic) {
        children = react_1["default"].createElement("em", null, children);
    }
    if (leaf.underline) {
        children = react_1["default"].createElement("u", null, children);
    }
    return react_1["default"].createElement("span", __assign({}, attributes), children);
};
var BlockButton = function (_a) {
    var format = _a.format, icon = _a.icon;
    var editor = slate_react_1.useSlate();
    return (react_1["default"].createElement(components_1.Button, { active: isBlockActive(editor, format), onMouseDown: function (event) {
            event.preventDefault();
            toggleBlock(editor, format);
        } },
        react_1["default"].createElement(components_1.Icon, null, icon)));
};
var MarkButton = function (_a) {
    var format = _a.format, icon = _a.icon;
    var editor = slate_react_1.useSlate();
    return (react_1["default"].createElement(components_1.Button, { active: isMarkActive(editor, format), onMouseDown: function (event) {
            event.preventDefault();
            toggleMark(editor, format);
        } },
        react_1["default"].createElement(components_1.Icon, null, icon)));
};
var initialValue = [
    {
        type: 'paragraph',
        children: [
            {
                text: 'In addition to nodes that contain editable text, you can also create other types of nodes, like images or videos.'
            },
        ]
    },
    {
        type: 'image',
        url: 'https://source.unsplash.com/kFrdX5IeQzI',
        children: [{ text: '' }]
    },
    {
        type: 'paragraph',
        children: [
            {
                text: 'This example shows images in action. It features two ways to add images. You can either add an image via the toolbar icon above, or if you want in on a little secret, copy an image URL to your keyboard and paste it anywhere in the editor!'
            },
        ]
    },
    {
        type: 'video',
        url: 'https://app-test.obs.cn-east-2.myhuaweicloud.com/textVideo202011131700.mp4',
        children: [{ text: '' }]
    },
];
exports["default"] = RichText;
var templateObject_1, templateObject_2, templateObject_3;
